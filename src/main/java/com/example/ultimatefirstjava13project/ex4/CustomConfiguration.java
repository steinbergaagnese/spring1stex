package com.example.ultimatefirstjava13project.ex4;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomConfiguration {

@Bean("bestDummyLogger")
    public DummyLogger generateDummyLogger(){
    return new DummyLogger();
}

@Bean
    public ListUtil listUtility(){
    return new ListUtil();
}

@Bean("StringUtility")
    public StringUtil stringUtil(){
    return new StringUtil();
}


}
