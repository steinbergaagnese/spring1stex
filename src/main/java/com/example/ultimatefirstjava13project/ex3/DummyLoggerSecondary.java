package com.example.ultimatefirstjava13project.ex3;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DummyLoggerSecondary implements DummyLogger{
    @Override
    public void sayHello() {
        log.info("Secondary logger!");
    }
}
