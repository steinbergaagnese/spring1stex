package com.example.ultimatefirstjava13project.SelfCheck1;

import org.springframework.stereotype.Component;

@Component
public class Person {

    private Gun lazer;

    public Person(Gun lazer) {
        this.lazer = lazer;
    }

    public int shoot(){
        return lazer.shoot();
    }

    public void setLazer(Gun lazer) {
        this.lazer = lazer;
    }
}
