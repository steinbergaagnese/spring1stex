package com.example.ultimatefirstjava13project.SelfCheck1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Lazer implements Gun {

    @Value("${lazer.shoot.sound:Pew Pew}")
    private String shootingSound;

    public int shoot() {
        log.info(shootingSound);
        return 5;
    }
}
