package com.example.ultimatefirstjava13project.SelfCheck1;

import com.example.ultimatefirstjava13project.ex6.CustomConfigComponent;
import com.example.ultimatefirstjava13project.ex6.CustomConfigComponent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShootingRange implements CommandLineRunner {

    private final Person person;
    private final Enemy enemy;
    private final List<Gun> weapons;
    private final CustomConfigComponent configComponent;

    @Override
    public void run(String... args) throws Exception {
        log.info(configComponent.toString());

        Random r = new Random();
        int low = 0;
        int high = weapons.size() - 1;
        int result = r.nextInt(high-low) + low;
        log.info(result+"");

        enemy.setWeapon(weapons.get(result));

        result = r.nextInt(high-low) + low;
        log.info(result+"");

        person.setLazer(weapons.get(result));

        if(person.shoot() > enemy.shoot()){
            log.info(person.getClass() + " wins.");
        }else{
            log.info(enemy.getClass() + " wins.");
        }
    }
}
