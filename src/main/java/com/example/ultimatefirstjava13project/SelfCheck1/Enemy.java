package com.example.ultimatefirstjava13project.SelfCheck1;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Enemy {

    private Gun weapon;

    public Enemy(@Qualifier("lazer") Gun weapon) {
        this.weapon = weapon;
    }

    public int shoot(){
        return weapon.shoot();
    }

    public void setWeapon(Gun weapon) {
        this.weapon = weapon;
    }
}
