package com.example.ultimatefirstjava13project.ex2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

@Service

public class CommandLineRunnerWithFieldInjection implements CommandLineRunner {
@Autowired
    private DummyLogger dummyLogger;

    @Override
    public void run(final String... args) throws Exception {
        dummyLogger.sayHello("Hello from CommandLineRunnerWithFieldInjection");
    }
}