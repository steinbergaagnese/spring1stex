package com.example.ultimatefirstjava13project.ex9;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface FileDataRepo extends JpaRepository<FileData, UUID> {



}
