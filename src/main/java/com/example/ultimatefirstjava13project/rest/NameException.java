package com.example.ultimatefirstjava13project.rest;


public class NameException extends Throwable {
    public NameException(String message) {
        super(message);
    }
}
