package com.example.ultimatefirstjava13project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UltimateFirstJava13ProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(UltimateFirstJava13ProjectApplication.class, args);
    }

}
